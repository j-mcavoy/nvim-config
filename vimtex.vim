let g:tex_flavor = 'latex'

autocmd FileType tex setlocal makeprg=tectonic\ --outdir\ output\ '%'
