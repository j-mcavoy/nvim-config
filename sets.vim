set shell=/bin/bash

let mapleader=","

set bg=light
set clipboard+=unnamedplus
set encoding=utf-8
set expandtab
set expandtab           " Insert spaces when TAB is pressed.
set ff=unix
set gdefault            " Use 'g' flag by default with :s/foo/bar/.
set go=a
set ignorecase
set ignorecase          " Make searching case insensitive
set linespace=0         " Set line-spacing to minimum.
set list                " Show problematic characters.
set magic               " Use 'magic' patterns (extended regular expressions).
set mouse=a
set nohlsearch
set nojoinspaces        " Prevents inserting two spaces after punctuation on a join (J)
set nostartofline       " Do not jump to first character with page commands.
set number              " Show the line numbers on the left side.
set number relativenumber
"set shiftwidth=2
set shiftwidth=4        " Indentation amount for < and > commands.
set showmatch           " Show matching brackets.
set smartcase
set smartcase           " ... unless the query has capital letters.
set spell
set splitbelow splitright
set splitright          " Vertical split to right of current.
set tabstop=4           " Render TABs using this many spaces.
set undodir=~/.local/share/nvim/undo
set undofile
set wildmode=longest,list,full


if !&scrolloff
    set scrolloff=3       " Show next 3 lines while scrolling.
endif
if !&sidescrolloff
    set sidescrolloff=5   " Show next 5 columns while side-scrolling.
endif
" Tell Vim which characters to show for expanded TABs,
" trailing whitespace, and end-of-lines. VERY useful!
if &listchars ==# 'eol:$'
    set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

" Relative numbering
function! NumberToggle()
    if(&relativenumber == 1)
        set nornu
        set number
    else
        set rnu
    endif
endfunc

syntax on

filetype plugin on
