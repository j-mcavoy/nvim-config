"Automatically start language servers.
let g:deoplete#enable_at_startup = 1
let g:LanguageClient_autoStart = 1
inoremap <silent><expr> <TAB>
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ deoplete#manual_complete()


let g:LanguageClient_serverCommands = {
            \ 'cpp': ['cquery', '--log-file=/tmp/cq.log'],
            \ 'c': ['cquery', '--log-file=/tmp/cq.log'],
            \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
            \ 'typescript': ['javascript-typescript-stdio'],
            \ 'typescript.jsx': ['javascript-typescript-stdio'],
            \ 'python': ['/usr/bin/pyls'],
            \ 'elm': ['elm-language-server', '--stdio'],
            \ 'sh': ['bash-language-server', 'start']
            \ }

nnoremap <silent> K :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient_textDocument_rename()<CR>
