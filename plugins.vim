call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-surround'
Plug 'jreybert/vimagit'
Plug 'bling/vim-airline'
Plug 'ap/vim-css-color'

Plug 'AndrewRadev/sideways.vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'AndrewRadev/switch.vim'

" https://github.com/jonhoo/configs
" Load plugins
" VIM enhancements
Plug 'editorconfig/editorconfig-vim'
Plug 'justinmk/vim-sneak'

" GUI enhancements
Plug 'itchyny/lightline.vim'
"Plug 'machakann/vim-highlightedyank'
Plug 'andymass/vim-matchup'

" Fuzzy finder
Plug 'airblade/vim-rooter'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'airblade/vim-rooter'

" Semantic language support
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Languages
Plug 'cespare/vim-toml'
Plug 'stephpy/vim-yaml'
Plug 'rust-lang/rust.vim'
Plug 'rhysd/vim-clang-format'

"Plug 'fatih/vim-go'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

Plug 'rust-lang/rust.vim'

Plug 'preservim/nerdtree'

Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin' |
            \ Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

Plug 'puremourning/vimspector'

Plug 'lervag/vimtex'

Plug 'tpope/vim-fugitive'

Plug 'sirtaj/vim-openscad'

Plug 'digitaltoad/vim-pug'

Plug 'sbdchd/neoformat'

Plug 'jiangmiao/auto-pairs'

Plug 'vimlab/split-term.vim'

call plug#end()
