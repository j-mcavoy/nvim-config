nmap <F1> <nop>
imap <F1> <nop>

" Search and Replace
nmap <Leader>s :%s//g<Left><Left>

" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
    nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif

" Open file menu
nnoremap <Leader>o :CtrlP<CR>
" Open buffer menu
nnoremap <Leader>b :Buffers<CR>
" Open most recently used files
nnoremap <Leader>f :CtrlPMRUFiles<CR>

cnoreabbrev W w
cnoreabbrev Wq wq
cnoreabbrev wQ wq
cnoreabbrev Q q

cnoreabbrev pu PlugUpdate
cnoreabbrev pU PlugUpgrade
cnoreabbrev pi PlugInstall

cnoreabbrev cu Co,Update
cnoreabbrev cU CocUpdateSync
cnoreabbrev ci CocInstall
cnoreabbrev cc CocConfig

cnoreabbrev cr Cargo run
cnoreabbrev ct Cargo test
cnoreabbrev cb Cargo build

cnoreabbrev pr CocCommand python.execInTerminal

cnoreabbrev nc e ~/.config/nvim/init.vim
cnoreabbrev ncv vsp ~/.config/nvim/init.vim
cnoreabbrev ncs sp ~/.config/nvim/init.vim


nnoremap c "_c

" Goyo plugin makes text more readable when writing prose:
map <leader>f :Goyo \| set bg=light \| set linebreak<CR>

" Replace ex mode with gq
map Q gq

" Check file in shellcheck:
map <leader>s :!clear && shellcheck %<CR>

" Replace all is aliased to S.
nnoremap S :%s//g<Left><Left>

" Open corresponding .pdf/.html or preview
map <leader>p :!opout <c-r>%<CR><CR>

" Save file as sudo on files that require root permission
"cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" vimling:
nm <leader>d :call ToggleDeadKeys()<CR>
imap <leader>d <esc>:call ToggleDeadKeys()<CR>a
nm <leader>i :call ToggleIPA()<CR>
imap <leader>i <esc>:call ToggleIPA()<CR>a
nm <leader>q :call ToggleProse()<CR>

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

vnoremap <ESC> <C-c>

nnoremap <leader>r :source $MYVIMRC<CR>

nmap <Space><Space> :b#<CR>

" insert new line on pair
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
