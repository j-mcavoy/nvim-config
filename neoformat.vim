le: g:neoformat_enabled_c = ['astyle']
let g:neoformat_enabled_cpp = ['astyle']
let g:neoformat_enabled_arduino = ['astyle']
let g:neoformat_enabled_cs = ['astyle']
let g:neoformat_enabled_java = ['astyle']


augroup fmt
  autocmd!
  autocmd BufWritePre * undojoin | Neoformat
augroup END
