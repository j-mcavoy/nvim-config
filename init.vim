" Imports
source $HOME/.config/nvim/plugins.vim

source $HOME/.config/nvim/coc.vim
source $HOME/.config/nvim/auto-pairs.vim
source $HOME/.config/nvim/languages.vim
source $HOME/.config/nvim/nerd-tree.vim
source $HOME/.config/nvim/neoformat.vim
source $HOME/.config/nvim/rooter.vim
source $HOME/.config/nvim/rustfmt.vim
source $HOME/.config/nvim/sets.vim
source $HOME/.config/nvim/sideways.vim
source $HOME/.config/nvim/splitterm.vim
source $HOME/.config/nvim/vimspector.vim
source $HOME/.config/nvim/vimtex.vim

source $HOME/.config/nvim/maps.vim

function! s:check_back_space() abort "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}

" Also highlight all tabs and trailing whitespace characters.
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+$\|\t/

hi def DOWColor    ctermfg=red    guifg=red
hi link DOWMatch DOWColor

set spell
hi clear SpellBad
hi SpellBad cterm=underline gui=undercurl
hi clear SpellCap

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
    echo "Downloading junegunn/vim-plug to manage plugins..."
    silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
    silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
    autocmd VimEnter * PlugInstall
endif

" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Enable Goyo by default for mutt writing
autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo | set bg=light
autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>

" Automatically deletes all trailing whitespace and newlines at end of file on save.
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritepre * %s/\n\+\%$//e

" When shortcut files are updated, renew bash and ranger configs with new material:
autocmd BufWritePost files,directories !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
" Update binds when sxhkdrc is updated.
autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
if &diff
    highlight! link DiffText MatchParen
endif

" Linting
"let b:ale_linters = ['shellcheck']

" " Spice netlists and text listings
"so <sfile>:p:h/syntax/spice.vim
